<?php

namespace Trieu\Bundle\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Doctrine\ORM\Tools\Pagination\Paginator;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Trieu\Bundle\ApiBundle\Entity\User;

/**
* @RouteResource("User")
*/
class UserController extends FOSRestController
{
    /**
     * @View()
     * 
     * @QueryParam(name="limit", requirements="\d+", default="100", allowBlank=false, description="Item count limit")
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="sort", requirements="(asc|desc)", allowBlank=false, default="asc", description="Sort direction")
     * @QueryParam(name="total_count", requirements="(false|true)", default="false", description="Returns total number of records with results")
     * 
     * @ApiDoc(
     *	section = "User Api",
     *  resource=true,
     *  description="Return all users",
     *  filters={
     *      {"name"="a-filter", "dataType"="integer"},
     *      {"name"="another-filter", "dataType"="string", "pattern"="(foo|bar) ASC|DESC"}
     *  }
     * )
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
    	// $data = array('firstname' => 'michael', 'lastname' => 'trieu', 'middlename' => 'victory');

    	// Initialize return data
    	$returnData = [];

    	$repo = $this->getDoctrine()->getRepository('TrieuApiBundle:User');

    	// $users = $repo->findAll();

    	// retrieve parameters
    	$limit = $paramFetcher->get('limit');
    	$page = $paramFetcher->get('page');
    	$sort = $paramFetcher->get('sort');
    	$total_count = $paramFetcher->get('total_count');

    	// Set up query statement
    	$qb = $repo->createQueryBuilder('u');  // $query = $em->createQuery('')

    	if (!empty($sort))
    		$qb->orderBy('u.id', $sort);
    	
    	// finalize query
    	$query = $qb->getQuery();

    	if (!empty($limit)){
    		$query->setMaxResults($limit);
    		$returnData['current_count'] = $limit;
    	}

    	// gets total count
    	if (!empty($total_count)){
    		$paginator = new Paginator($query);
    		$returnData['total_count'] = count($paginator);
    	}

    	$users = $query->getResult();

    	$returnData['users'] = $users;

    	$view = $this->view($returnData, 200)
    		->setTemplate("TrieuApiBundle:User:get_users.html.twig")
    		->setTemplateVar('users');

    	return $this->handleView($view);

        // return $this->render('TrieuApiBundle:User:get_users.html.twig', array(
        //     // ...
        // ));
    }


    /**
    * @View()
    */
    public function getAction($id)
    {
    	// $data = array('id' => $id);
    	$repo = $this->getDoctrine()->getRepository('TrieuApiBundle:User');

    	$user = $repo->find($id);

    	$view = $this->view($user, 200);
    		// ->setTemplate("TrieuApiBundle:User:get_users.html.twig")
    		// ->setTemplateVar('user');

    	return $this->handleView($view);

    }

    /*
    */


    /**
    * @view()
    * 
	* @RequestParam(name="first_name", requirements="[A-Za-z]+", nullable=false, description="string")
    * @RequestParam(name="last_name", requirements="[A-Za-z]+", nullable=false, description="string") 
    * @RequestParam(name="company_name", requirements="[A-Za-z0-9 ]+", nullable=true, description="string")
    * @RequestParam(name="address", requirements="[A-Za-z0-9 ]+", nullable=false, description="string")
    * @RequestParam(name="city", requirements="[A-Za-z ]+", nullable=false, description="string")
    * @RequestParam(name="county", requirements="[A-Za-z ]+", nullable=false, description="string")
    * @RequestParam(name="state", requirements="[A-Za-z]{2}", nullable=false, description="string")
    * @RequestParam(name="zip", requirements="\d+", nullable=false, description="int")
    * @RequestParam(name="phone1", requirements="\d+", nullable=false, description="int")
    * @RequestParam(name="phone2", requirements="\d+", nullable=true, description="int")
    * @RequestParam(name="email", requirements="[A-Za-z0-9@.]+", nullable=false, description="string")
    * @RequestParam(name="web", requirements="[A-Za-z0-9]+", nullable=true, description="string")
    *
    *
    * @ApiDoc(
    * 	description = "Add a new user",
    * 	resource = true,
    *	section = "User Api",
	* 	parameters = {
	* 		{"name"="first_name", "dataType"="string", "required"=true, "description"="category id"}
	*  	}
    * )
    *
    * @param ParamFetcher $paramFetcher
    */
    public function postAction(ParamFetcher $paramFetcher)
    {
    	$postdata = $paramFetcher->all();

    	$user = new User();

    	foreach ($postdata as $key => $value){
    		// var_dump($key.' : '.$value);

    		$customSetterMethod = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));

    		$user->$customSetterMethod($value);
    	}
    	// var_dump($user);

    	$em = $this->getDoctrine()->getManager();
    	$em->persist($user);
    	$em->flush();

    	$view = $this->view($user, 200);

    	return $this->handleView($view);
    }

}