<?php 
// src/AppBundle/Controller/Controller/ResumeController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
* @Route("/resume")
*/
class ResumeController extends Controller
{	

	protected $address = 
		array(
			'street' => '4769 Nogal Street',
			'city' => 'San Diego',
			'state' => 'California',
			'zip' => 92102,
			'phone' => '(619) 306-1950',
			'email' => 'michaelvtrieu@gmail.com'
		);
	/**
	* @Route("/fancy")
	*/
	public function fancyResume()
	{
		return $this->render('resume/fancy.html.twig',
			array(
				'pagetitle' => 'Fancy | Resume - MVT'
			));
	}

	/**
	* @Route("/", name="resume") // default
	* @Route("/formal")
	*/
	public function formalResume()
	{
		// dump($this->address);
		return $this->render('resume/formal.html.twig',
			array(
				'pagetitle' => 'Resume | MVT',
				'address' => $this->address
			));
	}
}
?>